# Django, React and GraphQL Stater

Django, React and GraphQL are really amazing combo for build fullstack web apps. This project also includes docker.


## Features

* Docker
* Automated Code linting and Formating
* Nginx proxy ( not implemented )

## Setup

There are two ways that you can setup this project.

* Use docker
* Manual

### Docker Setup

Make sure that docker and docker-compose are installed.

```bash
$ docker-compose up --build
```

### Manual Setup

#### Api Setup

This starter uses the default sqlite database. Make sure to setup your database of choice before running the migrations.

```bash
$ cd api
$ pipenv install
$ pipenv run python manage.py migrate
$ pipenv run python manage.py runserver
```
